'use strict';

const {
  Errors: { ForbiddenError },
} = require('finale-rest');

const {
  API: {
    KEY,
    HEADERS: { X_API_KEY },
  },
} = require('../../config');

module.exports = (req, res, context) => {
  const header_api_key = req.header(X_API_KEY);

  if (header_api_key !== KEY) {
    throw ForbiddenError('Wrong API key');
  }

  return context.continue;
};
