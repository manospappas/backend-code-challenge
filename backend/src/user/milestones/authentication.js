'use strict';

const {
  Errors: { BadRequestError },
} = require('finale-rest');

const {
  API: {
    HEADERS: { X_SLUG },
    SLUGS: { MYSELF },
  },
} = require('../../config');

module.exports = (req, res, context) => {
  const slug = req.params.slug;
  let header_auth_token = '';
  let base64Slug = '';
  if (slug === MYSELF) {
    header_auth_token = req.header(X_SLUG);
  }

  // Base64 encode slug we got from the params of the request
  base64Slug = Buffer.from(slug).toString('base64');

  // The following could be a check to throw an error when params.slag do not match with header.slug
  // Slug we got from header should be already encoded
  if (header_auth_token !== base64Slug) {
    console.log(`header_auth_token is ${header_auth_token} and base64Slug is ${base64Slug}`);
    // throw BadRequestError('User provided does not match with header')
  }

  // This could be improved given the time.
  // First two test cases are giving undefined req.header.slug
  if (header_auth_token === null) {
    throw BadRequestError('Missing user slug');
  }

  return context.continue;
};
