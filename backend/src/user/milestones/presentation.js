'use strict';

const axios = require('axios');

const {
  STAR_WARS_API: {
    BASE_URL,
    ENDPOINTS: { PEOPLE },
  },
} = require('../../config');

module.exports = async (req, res, context) => {
  const char_ids_list = context.instance.dataValues.favourites;
  let results = [];

  for (let favourite in char_ids_list) {
    let result = await axios.get(`${BASE_URL}/${PEOPLE}/${char_ids_list[favourite]}`);
    results.push(result.data);
  }
  context.instance.dataValues.favouritesDetails = results;

  return context.continue;
};
