'use strict';

require('dotenv').config({ path: `${__dirname}/test.env` });

const request = require('supertest');

jest.mock('../src/config');

const {
  API: { KEY },
} = require('../src/config');

describe('User Resource', () => {
  let database = null;
  let server = null;

  let agent = null;

  beforeAll(async () => {
    database = require('./helpers/database');
    await database.start();
    server = require('../src/server');
    const app = await server.start();
    agent = request.agent(app);
    agent.set({ 'x-api-key': KEY });
  });

  afterAll(async () => {
    await server.stop();
    await database.stop();
  });

  describe('List Operation - GET /users', () => {
    test('should return empty array', async () => {
      await agent
        .get('/users')
        .expect(200)
        .expect('Content-Type', /json/)
        .then((res) => {
          expect(Array.isArray(res.body)).toBeTruthy();
          expect(res.body.length).toEqual(0);
        });
    });

    test('GET /specific-user | should return details as json', async () => {
      const providedSlug = 'me';

      await agent
        .get(`/users/${providedSlug}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .then((res) => {
          expect(res.body.length).toBe(1);
          expect(res.body.slug).toBe(providedSlug);
        });
    });

    test('should return status 404 when slug is uknown', async () => {
      const providedSlug = 'some-slug';

      await agent
        .get(`/users/${providedSlug}`)
        .expect(404)
        .expect('Content-Type', /json/)
        .then((res) => {
          expect(res.body.message).toBe('Not Found');
          expect(Array.isArray(res.body.errors)).toBeTruthy();
        });
    });
  });

  describe('List Operation - PUT /users', () => {
    test('PUT /specific-user | should return updated user details as json', async () => {
      const providedSlug = 'me';

      const data = {
        favourites: ['3', '4'],
      };

      await agent
        .put(`/users/${providedSlug}`)
        .send(data)
        .expect(200)
        .then(async (res) => {
          expect(res.body.slug).toBe(providedSlug);
          expect(res.body.slug).toBe(providedSlug);
          expect(Array.isArray(res.body.favourites)).toBeTruthy();
          expect(res.body.favourites.length).toEqual(data.favourites.length);
          expect(res.body.favourites).toEqual(data.favourites);
        });
    });

    test('should return status 500 when body is malformed', async () => {
      const providedSlug = 'me';

      const data = {
        favourites: { wrong: '3' },
      };

      await agent
        .put(`/users/${providedSlug}`)
        .send(data)
        .expect(500)
        .then(async (res) => {
          expect(res.body.message).toBe('internal error');
          expect(res.body.errors[0]).toBe('values.map is not a function');
        });
    });
  });
});
